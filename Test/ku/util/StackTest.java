package ku.util;

import static org.junit.Assert.*;

import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;
/**
 * StackTest is test stackfactory for find bug.
 * @author Napong D
 *
 */
public class StackTest {

	Stack stack;
		
	@Before
	public void setUp(){
		//set stack type 0 or 1
		StackFactory.setStackType( 1 ) ;
		stack = StackFactory.makeStack( 2 );
	}

	@Test
	public void newStackIsEmpty() {
		assertTrue( stack.isEmpty() );
		assertFalse( stack.isFull() );
		assertEquals( 0, stack.size() );
	}

	
	/** pop() should throw an exception if stack is empty */
	@Test( expected = java.util.EmptyStackException.class )
	public void testPopEmptyStack() {
		Assume.assumeTrue( stack.isEmpty() );
		stack.pop();
		// this is unnecessary. For documentation only.
		fail("Pop empty stack should throw exception");
	}

	@Test
	public void testPeek(){
		stack.push("q");
		stack.push("w");
		assertEquals( "w", stack.peek() );
		assertEquals( 2, stack.size() );
	}
	
	@Test
	public void testPop(){
		stack.push("q");
		stack.push("w");
		assertEquals( "w", stack.pop() );
		assertEquals( 1, stack.size() );
	}
	
	@Test
	public void testCapacity(){
		assertEquals( 2, stack.capacity() );
	}
	
	@Test( expected = java.util.EmptyStackException.class )
	public void testPush(){
		stack.push("q");
		stack.push("w");
		stack.push("e");
		fail("Push empty stack should throw exception");
		
	}
	
	@Test( expected=IllegalStateException.class )
	public void testFull(){
		stack = StackFactory.makeStack( 5 );
		assertTrue(stack.isEmpty());
		String [] character = {"a","b","c","d","e"};
		for(String s : character){
			stack.push(s);
			assertNotNull(stack.peek());
			assertFalse(stack.isEmpty());
		}
		assertEquals(5,stack.size());
		assertTrue(stack.isFull());
		stack.push("f");
	} 
	
	@Test( expected=IllegalArgumentException.class )
	public void testPushNull(){
		stack.push(null);
	}
	
	@Test
	public void testIsempty(){
		assertTrue( stack.isEmpty());
		assertFalse( stack.isFull() );
		assertEquals(0,stack.size());

	}
	
	@Test
	public void testPeekAndPop(){
		stack = StackFactory.makeStack( 3 );
		stack.push("q");
		assertEquals( "q", stack.peek() );
		stack.push("w");
		stack.push("e");
		assertEquals("e",stack.pop());
		assertEquals(2,stack.size());
		assertEquals("w",stack.peek());
		assertEquals(2,stack.size());
		assertEquals("w",stack.pop());
		assertEquals("q",stack.peek());
		
	}
	

	@Test
	public void TestIsFull(){
		stack.push("q");
		stack.push("w");
		assertTrue(stack.isFull());
		assertEquals(stack.size(),stack.capacity());
	}

}
